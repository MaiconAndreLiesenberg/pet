<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
     <div class="container">
       <c:url value="/home" var="home" />
       <a class="navbar-brand" href="${home}">Pets</a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
       </button>
       <div class="collapse navbar-collapse" id="navbarResponsive">
         <ul class="navbar-nav ml-auto">
           <c:choose>
			    <c:when test="${func}">
			    	<li class="nav-item active">
		           	 <c:url value="/pet" var="pet" />
		             <a class="nav-link" href="${pet}">Pet
		               <span class="sr-only">(current)</span>
		             </a>
		            </li>
		            <li class="nav-item active">
		           	 <c:url value="/admin/aprovacao" var="aprovacao" />
		             <a class="nav-link" href="${aprovacao}">Aprova��o
		               <span class="sr-only">(current)</span>
		             </a>
		            </li>
			    	<li class="nav-item">
			         <c:url value="/admin/tipo" var="tipo" />
			         <a class="nav-link" href="${tipo}">Categoria</a>
			        </li>
			        <li class="nav-item">
			         <c:url value="/admin/raca" var="raca" />
			         <a class="nav-link" href="${raca}">Ra�a</a>
			        </li>
			 	</c:when>
			 	<c:when test="${admin}">
			 		<li class="nav-item active">
		           	 <c:url value="/pet" var="pet" />
		             <a class="nav-link" href="${pet}">Pet
		               <span class="sr-only">(current)</span>
		             </a>
		            </li>
		            <li class="nav-item active">
		           	 <c:url value="/admin/aprovacao" var="aprovacao" />
		             <a class="nav-link" href="${aprovacao}">Aprova��o
		               <span class="sr-only">(current)</span>
		             </a>
		            </li>
			 		<li class="nav-item">
			       	 <c:url value="/admin/funcionario" var="funcionario" />
			         <a class="nav-link" href="${funcionario}">Funcionario</a>
			        </li>
			        <li class="nav-item">
			         <c:url value="/admin/tipo" var="tipo" />
			         <a class="nav-link" href="${tipo}">Categoria</a>
			        </li>
			        <li class="nav-item">
			         <c:url value="/admin/raca" var="raca" />
			         <a class="nav-link" href="${raca}">Ra�a</a>
			        </li>
			 	</c:when>
			 	<c:when test="${user}">
			 		<li class="nav-item active">
		           	 <c:url value="/pet" var="pet" />
		             <a class="nav-link" href="${pet}">Pet
		               <span class="sr-only">(current)</span>
		             </a>
		            </li>
			 	</c:when>
		        <c:otherwise>
		        	 <c:url value="/login" var="login" />
			         <a class="nav-link" href="${login}">Login/Cadastro</a>
		         </c:otherwise>
			</c:choose>
        <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      ${pageContext.request.userPrincipal.name}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
      <c:url value="/perfil" var="perfil" />
      <a class="dropdown-item" href="${perfil}">Meu perfil</a>
      <c:url value="/j_spring_security_logout" var="logoutUrl" />
      <a class="dropdown-item" href="${logoutUrl}">Sair</a>
    </div>
  </li>
      </ul>
    </div>
  </div>
</nav>
