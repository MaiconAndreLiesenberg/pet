<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script>

$(document).ready(function() {
	$('#acao').on('change', function() {
		console.log(this.value);
		if(this.value == '1'){
			$('#valorDiv').append(' <div id="valor">'
	                  +'<div class="mx-auto col-sm-10">'
	                  + '<label for="input2EmailForm" class="form-control-label">Valor</label>'
	                    +'<input type="text" class="form-control"  name="valor" placeholder="Valor" required>'
	                   +'</div></div>'
					);
		}else{
			$("#valor").remove();
		}
	})
});
	
</script>
<body>
	<%@ include file="../template/navbar.jsp" %>
	<div class="container">
		<div class="mt-5" style="padding-top: 30px">
		<h2 style="text-align:center">Cadastro Pet</h2>
		<form role="form" action="cadastrarPet" method='POST'>
           	<div class="form-group">
                   <div class="mx-auto col-sm-10">
                   		<label for="input2EmailForm" class="form-control-label">Nome</label>
						<input type="text" class="form-control" name="nome" placeholder="Nome" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
                   		<label for="input2EmailForm" class="form-control-label">Tamanho</label>
						<input type="text" class="form-control" name="tamanho" placeholder="Tamanho" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
                   		<label for="input2EmailForm" class="form-control-label">Idade</label>
						<input type="text" class="form-control"  name="idade" placeholder="Idade" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
                   		<label for="input2EmailForm" class="form-control-label">Descricao</label>
						<input type="text" class="form-control"  name="descricao" placeholder="Descricao" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
                   		<label for="input2EmailForm" class="form-control-label">Observa��o</label>
						<input type="text" class="form-control"  name="observacao" placeholder="Observa��o" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
						<label for="input2EmailForm" class="form-control-label">Categoria</label>
						<select class="form-control" name="tipoId" required="required">
							<option value="">Selecione um Tipo</option>
							<c:forEach var="tipo" items="${tipos}" >
							  <option value="${tipo.id}">${tipo.nome}</option>
					        </c:forEach>
				        </select>
                   </div>
               </div>
               <div class="form-group">
                   
                   <div class="mx-auto col-sm-10">
                   		<label for="input2EmailForm" class="form-control-label">Ra�a</label>
                       <select class="form-control" name="racaId" required="required">
							<option value="">Selecione uma Raca</option>
							<c:forEach var="raca" items="${racas}" >
							  <option value="${raca.id}">${raca.nome}</option>
					        </c:forEach>
				        </select>
                   </div>
               </div>
                <div class="form-group">
                   <div class="mx-auto col-sm-10">
                   		<label for="input2EmailForm" class="form-control-label">Objetivo</label>
                       <select class="form-control" id="acao" name="vendaForm" required="required">
							<option value="1">Venda</option>
							<option value="2">Doa��o</option>
				        </select>
                   </div>
               </div>
               <div class="form-group" id="valorDiv">
					<div id="valor">
						<div class="mx-auto col-sm-10">
							<label for="input2EmailForm" class="form-control-label">Valor</label>
						    <input type="text" class="form-control" name="valor" placeholder="Valor" required>
						</div>
                	</div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10 pb-3 pt-2">
                       <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Cadastrar</button>
                   </div>
               </div>
           </form>
      </div>
    </div>
</body>
</html>