<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="../template/navbar.jsp" %>
	<div class="container">
      	<div class="row" style="padding-top:30px">
				<!-- <h3 class="mt-5">Nenhuma consulta cadastrada!</h3> -->
				<input type="button" class="mt-5 btn" value="Cadastro" onClick="location.href='/Consultorio/admin/raca/cadastro'"/>
		</div>
		<table class="table">
		  <thead>
		    <tr>
		      <th scope="col">ID</th>
		      <th scope="col">Nome</th>
		      <th scope="col">Descri��o</th>
		      <th scope="col">Tipo</th>
		      <th scope="col">A��es</th>
		    </tr>
		  </thead>
		  <tbody>
		  <c:forEach var="raca" items="${racas}">
		  	<tr>
		      <td>${raca.id}</td>
		      <td>${raca.nome}</td>
		      <td>${raca.descricao}</td>
		      <td>${raca.tipo.nome}</td>
		      <td>
		      	<input type="button" class="btn" value="Editar" onClick="location.href='/Consultorio/admin/raca/editar?racaId=${raca.id}'"/>
		      	<input type="button" class="btn" value="Remover" onClick="location.href='/Consultorio/admin/raca/remover?racaId=${raca.id}'"/>
		      </td>
		    </tr>
		  </c:forEach>
		  </tbody>
		</table>
    </div>
</body>
</html>