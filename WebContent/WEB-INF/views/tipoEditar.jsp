<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="../template/navbar.jsp" %>
	<div class="container">
		<div class="mt-5" style="padding-top: 30px">
			<form action="editarTipoForm" method='POST' >
				<input type='hidden' name='id'value="${tipo2.id}">
				
				<div class="form-group">
	                  <div class="mx-auto col-sm-10">
		                  <label for="nome" class="form-control-label">Nome</label>
	                      <input type="text" class="form-control" id="nome" name='nome'value="${tipo2.nome}" required>
	                  </div>
	              </div>
	               
	              <div class="form-group">
	                  <div class="mx-auto col-sm-10 pb-3 pt-2">
	                      <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Cadastrar</button>
	                  </div>
	              </div>
			</form>
		</div>
	</div>
</body>
</html>