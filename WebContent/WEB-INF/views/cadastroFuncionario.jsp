<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="../template/navbar.jsp" %>
	cadastro funcionario
	<div class="container">
	<div class="mt-5">
		<h2 style="text-align:center">Cadastro Funcion�rio</h2>
		<form class="py-2" role="form" action="cadastrarFuncionario" method='POST'>
            <div class="form-group">
                <div class="mx-auto col-sm-10">
                	<label for="inputEmailForm" class="form-control-label">Nome</label>
                    <input type="text" class="form-control" name="nome" placeholder="Nome" required>
                </div>
            </div>
            <div class="form-group">
                <div class="mx-auto col-sm-10">
	                <label for="inputEmailForm" class="form-control-label">Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group">
                <div class="mx-auto col-sm-10">
	                <label for="inputEmailForm" class="form-control-label">Endere�o</label>
                    <input type="text" class="form-control" name="endereco" placeholder="Endere�o" required>
                </div>
            </div>
            <div class="form-group">
                <div class="mx-auto col-sm-10">
	                <label for="inputEmailForm" class="form-control-label">Username</label>
                    <input type="text" class="form-control" name="username" placeholder="Username" required>
                </div>
            </div>
            <div class="form-group">
                <div class="mx-auto col-sm-10">
	                <label for="inputPasswordForm" class="form-control-label">Senha</label>
                    <input type="password" class="form-control" id="inputPasswordForm" name="password" placeholder="Senha" required>
                </div>
            </div>
            <div class="form-group">
                <div class="mx-auto col-sm-10 pb-3 pt-2">
                    <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Cadastrar</button>
                </div>
            </div>
        </form>
		</div>
	</div>
</body>
</html>