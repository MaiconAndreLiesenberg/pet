<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
 	<script type="text/javascript" src="<c:url value="/resources/js/cidades-estados-1.4-utf8.js" />"></script>
<title>Login Page</title>
<style>
 .error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}

#login-box {
	width: 300px;
	padding: 20px;
	margin: 100px auto;
	background: #fff;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border: 1px solid #000;
} 
</style>
</head>
<body>
	<%@ include file="../template/navbar.jsp" %>
	<div class="container py-5">
    <h2 class="text-center pb-2"></h2>
    <div class="row">
        <div class="col-lg-6 col-12 pb-3">
            <div class="card">
                <div class="card-body">
                    <h2 class="text-center mb-4">Login</h2>
                    <c:if test="${not empty error}">
						<div class="error">${error}</div>
					</c:if>
                    <form class="py-2" role="form" action="<c:url value='/j_spring_security_check' />" method='POST'>
                        <div class="form-group">
                            <label for="inputEmailForm" class="sr-only form-control-label">Username</label>
                            <div class="mx-auto col-sm-10">
                                <input type="text" class="form-control" id="inputEmailForm" name="username" placeholder="Username" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPasswordForm" class="sr-only form-control-label">Senha</label>
                            <div class="mx-auto col-sm-10">
                                <input type="password" class="form-control" id="inputPasswordForm" name="password" placeholder="Senha" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10 pb-3 pt-2">
                                <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Logar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-12 pb-3">
            <div class="card">
                <div class="card-body">
                    <h2 class="text-center mb-4">Registrar-se</h2>
                    <c:if test="${not empty erro}">
						<div class="error">${erro}</div>
					</c:if>
                    <form role="form" action="cadastroUsuario" method='POST'>
                    	<div class="form-group">
                            <div class="mx-auto col-sm-10">
                            	<label for="input2EmailForm" class="form-control-label">Nome</label>
                                <input type="text" class="form-control" name="nome" placeholder="Nome" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10">
	                            <label for="input2EmailForm" class="form-control-label">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10">
                            	<label class="form-control-label">Telefone</label>
                                <input type="text" class="form-control" name="telefone" placeholder="Telefone" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10">
	                            <label for="input2PasswordForm" class="form-control-label">Estado</label>
                                <select class="form-control" id="estado4" name="estado" required="required"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10">
	                            <label for="input2PasswordForm" class="form-control-label">Cidade</label>
                                <select class="form-control" id="cidade4" name="cidade" required="required"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10">
	                            <label for="input2EmailForm" class="form-control-label">Endereço</label>
                                <input type="text" class="form-control"  name="endereco" placeholder="Endereço" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10">
	                            <label for="input2EmailForm" class="form-control-label">Username</label>
                                <input type="text" class="form-control"  name="username" placeholder="Username" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10">
	                            <label for="input2PasswordForm" class="form-control-label">Senha</label>
                                <input type="password" class="form-control" name="password" placeholder="password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mx-auto col-sm-10 pb-3 pt-2">
                                <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Registrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
		new dgCidadesEstados({
		  cidade: document.getElementById('cidade4'),
		  estado: document.getElementById('estado4')
		})
	</script>
</div>
</body>
</html>