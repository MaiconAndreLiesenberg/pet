<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<html>
<head>
<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	<%@ include file="../template/navbar.jsp" %>
    <div class="container">
      	<div class="row" style="padding-top:30px">
				<!-- <h3 class="mt-5">Nenhuma consulta cadastrada!</h3> -->
				<input type="button" class="mt-5 btn" value="Cadastrar" onClick="location.href='/Consultorio/admin/funcionario/cadastro'"/>
		</div>
		<table class="table">
		  <thead>
		    <tr>
		      <th scope="col">ID</th>
		      <th scope="col">Nome</th>
		      <th scope="col">Email</th>
		      <th scope="col">Endere�o</th>
		      <th scope="col">A��es</th>
		    </tr>
		  </thead>
		  <tbody>
		  <c:forEach var="funcionario" items="${funcionarios}">
		  	<tr>
		      <td>${funcionario.id}</td>
		      <td>${funcionario.usuario.nome}</td>
		      <td>${funcionario.usuario.email}</td>
		      <td>${funcionario.usuario.endereco}</td>
		      <td>
		      	<input type="button" class="btn" value="Remover" onClick="location.href='/Consultorio/admin/funcionario/remover?funcId=${funcionario.id}'"/>
		      </td>
		    </tr>
		  </c:forEach>
		  </tbody>
		</table>
    </div>
</body>
</html>