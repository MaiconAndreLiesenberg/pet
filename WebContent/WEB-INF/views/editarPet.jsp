<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="../template/navbar.jsp" %>
	<div class="mt-5" style="padding-top: 30px">
		<h2 style="text-align:center">Editar Pet</h2>
		<form role="form" action="editarPet" method='POST'>
			<input type='hidden' name='id'value="${pet2.id}">
           	<div class="form-group">
                   
                   <div class="mx-auto col-sm-10">
                   		<label class="form-control-label" >Nome</label>
                       <input type="text" class="form-control" name="nome" value="${pet2.nome}" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
                	   <label for="input2EmailForm" class="form-control-label">Tamanho</label>
                       <input type="text" class="form-control" name="tamanho" value="${pet2.tamanho}" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
            	       <label for="input2EmailForm" class="form-control-label">Idade</label>
                       <input type="text" class="form-control"  name="idade" value="${pet2.idade}" placeholder="Idade" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
        	           <label for="input2EmailForm" class="form-control-label">Descri��o</label>
                       <input type="text" class="form-control"  name="descricao" value="${pet2.descricao}" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
    	               <label for="input2EmailForm" class="form-control-label">Observa��o</label>
                       <input type="text" class="form-control"  name="observacao" value="${pet2.observacao}" required>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
	                   <label for="input2EmailForm" class="form-control-label">Categoria</label>
                       <select class="form-control" name="tipoId" required="required">
							<option value="${pet2.tipo.id}">${pet2.tipo.nome}</option>
							<c:forEach var="tipo2" items="${tipos2}" >
							  <option value="${tipo2.id}">${tipo2.nome}</option>
					        </c:forEach>
				        </select>
                   </div>
               </div>
               <div class="form-group">
                   <div class="mx-auto col-sm-10">
    	               <label for="input2EmailForm" class="form-control-label">Ra�a</label>
                       <select class="form-control" name="racaId" required="required">
							<option value="${pet2.raca.id}">${pet2.raca.nome}</option>
							<c:forEach var="raca" items="${racas}" >
							  <option value="${raca.id}">${raca.nome}</option>
					        </c:forEach>
				        </select>
                   </div>
               </div>
               <c:if test="${not empty pet2.valor}">
                   <div class="mx-auto col-sm-10">
					   <label for="input2EmailForm" class="form-control-label">Valor</label>
                       <input type="text" class="form-control" name="valor" value="${pet2.valor}" required>
                   </div>
			   </c:if>
               <div class="form-group">
                   <div class="mx-auto col-sm-10 pb-3 pt-2">
                       <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Salvar</button>
                   </div>
               </div>
           </form>
	</div>
</body>
</html>