<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="../template/navbar.jsp" %>
	<div class="container">
		<c:if test="${not empty msg}">
			<div>${msg}</div>
		</c:if>
		<div class="row mt-5">
		    <c:url value="/perfil/trocarSenha" var="alteraSenha" />
			<form action="${alteraSenha}" method='POST'>
	
				<table>
					<tr>
						<td>Senha antiga</td>
						<td><input type="password" name='antiga'></td>
					</tr>
					<tr>
						<td>Nova senha</td>
						<td><input type='password' name='nova'></td>
					</tr>
					<tr>
						<td colspan='2'><input name="submit" type="submit"
							value="submit" /></td>
					</tr>
				</table>
	
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
	
			</form>
		</div>
	</div>
</body>
</html>