<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="../template/navbar.jsp" %>
	<div class="container">
		<!-- <div class="row" style="padding-top:30px">
			<div class="col">
				<h3 class="mt-5">Nenhuma consulta cadastrada!</h3>
				<input type="button" class="mt-5 btn d-block mx-auto" value="Alterar a senha" onClick="location.href='/Consultorio/perfil/alteraSenha'"/>
			</div>
		</div> -->
		<div class="mt-5" style="padding-top:30px"></div>
		<c:if test="${not empty msg}">
			<div class="row">
		    	<div class="col">
					<div class="d-block mx-auto">${msg}</div>
				</div>
			</div>
		</c:if>
		<div class="row">
		    <div class="col">
			    <c:url value="/perfil/editarPerfil" var="editarPefil" />
				<form action="${editarPefil}" method='POST'>
					<div class="form-group">
		                <div class="mx-auto col-sm-10">
		                	<label for="nome" class="form-control-label">Nome</label>
		                    <input type="text" class="form-control" name="nome" value="${usuario.nome}" required>
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <div class="mx-auto col-sm-10">
		                	<label for="email" class="form-control-label">Email</label>
		                    <input type="text" class="form-control" name="email" value="${usuario.email}" required>
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <div class="mx-auto col-sm-10">
		                	<label for="endereco" class="form-control-label">Endere�o</label>
		                    <input type="text" class="form-control" name="endereco" value="${usuario.endereco}" required>
		                </div>
		            </div>
		            
		            <div class="form-group">
	                   <div class="mx-auto col-sm-10 pb-3 pt-2">
	                       <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Submit</button>
	                   </div>
	               </div>
		
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
		
				</form>
		    </div>
		</div>
	</div>
	
</body>
</html>