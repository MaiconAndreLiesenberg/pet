<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ include file="../template/navbar.jsp" %>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
* {
    box-sizing: border-box;
}

body {
    background: #eeeeee;
    font-family: 'Calibri';
}

.cardpet {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    transition: 0.3s;
    background-color: white;
    overflow: hidden;
    /* height: 100px; */
}

.cardpet:not(:first-child) {
    margin-top: 4px;
}

/* On mouse-over, add a deeper shadow */

.cardpet:hover {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
}

/* Add some padding inside the card container */

.contentv {
    float: left;
    padding: 2px 16px;
    overflow: hidden;
    padding: 9px;
    width: calc(100% - 103px);
}

.contentv .vinte > div {
    float: left;
    width: 20%;
}

.contentv .cinquenta > div {
    float: left;
    width: 50%;
}

.contentv > div {
    overflow: hidden;
}

.contentv > div:not(:first-child) {
    margin-top: 10px;
}

.img-card {
    float: left;
    height: 100px;
    padding: 9px;
}

.img-card>img {
    height: 100%;
}
</style>
<body>
	<%@ include file="../template/navbar.jsp" %>
	<div class="container">
      	<div class="row" style="padding-top:30px">
				<!-- <h3 class="mt-5">Nenhuma consulta cadastrada!</h3> -->
				<input type="button" class="mt-5 btn" value="Cadastrar" onClick="location.href='/Consultorio/pet/cadastro'"/>
		</div>
		<br>
		<c:choose>
		    <c:when test="${not empty aprovacoes}">
				<div class="row">
					<div class="col-md-11">
						<div id="lista">
						<c:forEach var="aprovacao" items="${aprovacoes}">
							<div class="cardpet">
						        <div class="img-card">
						            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFhUXGR0bGRgYGBodHRsdGhgXGBodGx0ZICggHyAlHxoeIjEiJSkrLi4uGiAzODMtNygtLisBCgoKDg0OGhAQGyslICUtLS0vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAL8BBwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAgMEBgcBAAj/xAA8EAABAgQEAwYEBQMEAgMAAAABAhEAAyExBAUSQVFhcQYTIoGRoTKxwfAHQtHh8RQjUhVicoKSoiUz0v/EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAAsEQACAgICAQIFAwUBAAAAAAAAAQIRAyESMQQTQQVCUWGBMqHwFSJxkbEU/9oADAMBAAIRAxEAPwBMnP5QS4CX0+GrsATQ7uXbzh/Kc1XOAfSlNis2LEMByr7wOx+VSEIBSNKUFWs1OpAZXmasPKKngs1WEqMuoqm3EMzWqBHzUcSmm4m7bXZosjEjxBKiVaidO/hKgabuRSnsC0pGJN3aWLksKq2HP5Rm+W50ZYWRMUFlQ9A5/TlFtyDCzZjT580pTQplm5agJH5eVK8ojJg47YKRYVTCm/RQA6P7UH7Quagrlq7xekKIsK6Te9nD24w1PlKSoqRMooFWgA35M5ekDcwzpQS4QWA8QIsWoD7C24jFJ3oqw2JADqdXnZmc/MnzMAUdrJKpszDIcqQCVKpooUpIDnn97wc3zJc7Cz5iSuS0pSqFqkEuG4tekZv2bx3dYhKlVSTpXR6K3+RjvweC54pTl96Ic1dI3JeOQJUxaVuyVEAhndLj1akSZbJlBAppAST0ACQPQmK2uWlUsd2oFKlgEcxpt1RBFEwhLKNQR4Qbbj5/zHBJ0i0yFj85ly1KSxfm4ruPvjHspznUVIICeZqLtQDk3mY9j8IFpUTpJ2BsCnfjYs28AZMjSdLaVE6UkahVIC992PmxhwSaJbovUlCFBkkgnYebqItDS0MyClRTcgWqXYtxpb2il5TOXKTMWtRKUkg3csW0pBZwNzxbhBrA9oCSVOyGCjq2ccOV+EKWNp6HyTDQmfEkDUkizDw0Y2pcQpBBOopbpYEufM/tAmXjAjU6WKVFlVaxfm9TEvAY1C0AvRn4Hg/O1PeCnQWTsPjkknSLhxX6Q3iZzV1AmrV5E+fygPmfgD1AATY3uW5Ab7NHZE1agkgajqqfn7FxxDROwtBJJUWKtOgHxGvB6nfbwiFFUp0qICzQlzR2DekJm4JSpWlawkPXc0INzwAMC8RhJbKQl6kUevhqx3YEQ0FheTikOzAMNt6mwFG5w/ipzJJS5IYsCyXFhvFUQuZqVqcAOW3DFh5MH9IP4UTNIP5T6gMBSB6FyTO4dM2YnvaJLkHVVuH2OUIkytSipUtJNASa0Fr3dzEzCSSgEKdjxLUo8DsdIUklSNWmzWBYF23sILa6GyLicuBOpZfVQAfDdwQLbH0iBlHZYYeZMWmZqCnJ4uSVW4MQIsEnG6gHRXTws7AwymagrqyQR7PTw7Rvh8nJidxZDgmRcxkA6DYgg0glPwMtUsqUzqbVSiqNUcwSOnlHJiUKI0l+e3DzrEPMMWtEohte4SLgPuece74/m48unpkPFZWJ2A0y5khKAr+8kqX5gJSGqSSCel472mkIEjEJDOmTpLXGkj6iD+WhJX3hSyUqccyw25V8zFT7ZYombigmgUjS3l+sb0tMmEGrTMwhcuOolk2DsHPIWc8qwVk5OrRMO4lrJH+KpUxOtPXTXoqLnNR7AHYZQCgSAocDa0eixZR2XUtKZgGtBAPC4FN3qfJubx6OPJ5WHlTYUzUZUxK3kKAUAkJJUTUVB1UbcU9d2GTsKmUr+1KKpakCWoB2Ol0oYbfEfIGCmIy2QhlhaqEhISaC5Dj796wMV2ikyU6T4wDUublje71JdxTqI8GNt1HZ0Mqee5c0xRCdJABZNQAeNGB5VtB3sqpaZJUtKinf/Jy3o1TBI56JiVESywqKAmh5OCW5t8oaTmUoSiE2JASCPhUS1Ru160epMbSnKUeLRGk7sM4bNyha+80pQCljahCr+gim9oO38kEolI71laiR4UkguDYk2EWDDIw+iV/VqSAEqmzEk/EEMC43SVKdgd4zXPpMtWIWuWEplzFlSQGoCS1BbpHX4fhQyR9SYpSfRaspmqmyZ2sJ/urlFgzaV+BSUjkVe8UXE4JcpZSosQQlXoDbk8HpWMGHMla06pet2ciwJH/sx8oj9ocylTp+qXqIUdRKgASSxY9Hbyj2IpJJIhofwuNxUgpHeeEGj2sU/JR3iw4HPnSoKBSqpZ/YP91jkvKzNlBxdIa+31ipYs92tiS4N/sxzZvEx5l1sLaNNkZmhIqPEA9xTYjxEB6ij7wn/WZBCbGtyBdtIJYs4c0MB+z2A7+WkpUHSoGo1AuHU4tX4dyHibj+zkxCkGQlHdqcTEgMd6kUSbUDOG5mPAeKEJOLezROVdBWfNlLUykjUW0niE16UJ+2j2ZZdL1gqPh0NpH5mU/o5+3hmdl6iEplJCS4LqvdyOnvSFTZ3dha1nUAw86Bxs1WEZq10N/cZy6cMQlRAKUmnRlKc/xsYTmOQqchMwhRACANgzJY8HD82aO4PGy9JUlTFLEIZj4qAbEeLejQiVjFq/ONSSGIDEFILnYLF/SLtrrQUSJ2NDBCx4nYgUsdAVWrEsRDWFxayQoOkFKiXsyQAenDqImy8JLUHLODq1K3O/Ji7MeA4QxIx4RqQZbpKnLJ0pLlyS4c1rE2pdIpaJgXMmSSEACZpcFVXIZ6cWbzaGpOASmatCHUpB1A/wDI+EAmhoDWHk5sElALAgEgPxenFqdLQ2nPJIWofCQWDPUFqtZw56OYN10LT7HsRKYudKSS4d+aQVcQ5NDd4H5Znw7zQz1YV4Epf0HvE7NcwQqXRGpVgFMAlx8RD13Lcop+PlKC1KlFRWeFAE0FC7tzAHtFQjemKUqL9iMUQs/CUsOLijmzw7l+JC3Op2qOhP7vFPwONnhI1pCgAPC1q8Cb0iflXeqmlSkrAf4SaF7eQc+h4RPAFOwzjMKkvpJDbvU02hqfhUpSpa6JZq9A9h0vw5Q/mGI7pKVTLksGFa0f5QmfN1SynUCVCwNQDzApfrEJUxsqWVzlKWooUtSEgJTqboNrfpvFlVLCZepanCqedoinABADCnCwaoAAgxLTKmyyhVQPoaMI9rwOEm3r8mVuL2AMpxAX3goySw6mA/ahIGJFk60DxEUuQXg1jMLLw5Pdj4jvuzvFZzyegrClcAOPEBhzv0MdOTKox4s0nkXJtAzI8OmUpfeokFPwq0LIcE6SClQ07kUb5RZsvmy5alIPiCi6SzjTpCC6tyS79eYiKpIUHoQKLDbEhnBr9loGZpLmSVgAsklgbioJBB4XpyMeVOXqvZmnRcJSpRCG0uAwAJYCgNBzHDaPQGw+sFJUnxAODyKQH5ihj0crivqWmgsuSJQQVzSdJewAuG4XcC7+Hyha0SFq7tSQlTlYVpD61JKSoP8AEQC4Bo/SjeOliYkiYlJQEtUsSoltPQsz3tziIJSFGWaCnB9OzdGDfWsFPsbB+NwUiRNSkTVBc4qSAn8oGlLkWcqL0YszGLVk34YihmT1NulIAJcN4iX24NAzEyEoXJnmW/dqKmNVB2SWHH833S+ZH2lwyvF38uvMD5m8e38PhjyYuT2/cymqZR+1X4ZKQpSpCVrkmWE6UkqWkhSSltZNLnhW0D0/hnNnYV0juJiCVJlkPqoPiU7xsac3lrUyTXoa9CQx8oiLzFHeBOpidm4EOA4av1j0tLok+d8zy2clP9NiEaZiXKeYq8Ruy+Tpm4lKFWDH3+/WNU/FeXLKJawQJiVGu7aSSPl6xnvZadoxGqtTsxNOUJrQ7NmwuQSwgVtyjPO32QpLqSA/FrxpGR44r8Kg31iH2ywAVKUBQ3BYmv6GJjobMY7KZ+uTM0KJKT5xqckCYn4/iIfiRQ04dRy3jGM4wKkqKgNNbVoeUWbsj2xKQJc08GVSnrHmfEPDcl6kFsuEq0zR1SFl0FRGpOkKB3ZRHzEMdx/d7sChSCVE/mHhIbg4B8zEaVmCmD22I3PP73iNie0RQrxJoqiC24SApiK7kk9eUeNFPqi217hHG4VKEkqAGsAFSU1pR+J5DlFVmolFSpadaVEuk70/NXiAamlYsOJz90HTpSp2BIUWe4FLsT0EAcTgSpTpQXo6ia0pswc8ybRcNdkyaImDVPKghai6C4SDQhibipIO8GJeHmqI1uGWUl9NKV4UcM4uFgw1KWJE1OsgagalgzOSPT3iXPx8ucnwKSli2oi55D23ipSb6Wgj9wdjcqKVhyCyQ5Buo1obswbziYjL5fdp1jWuWkgFrvfodv4hhM5cxiVFQKmBNrNBDvApXdguQQ44DiBv98YmUpCoAysQuYoJThwk61VIIZn1EFuR534wTy/FLI0rSkKA1Em+7Pw4VicnDd2NRKvhV5lQ24N9IYyVQVqXQhwHs4CjQ832glKLWkSkxufNJKkkCwLixc1HyqeMEcFjgFDUblyBbdg8dzHL0mX/AGyQtZGlgCU8XJ2pV9or+HSuWXXOK3soAAEv+UcgC5N34RPFSQ9xLVjppmjQFMQHDNWtA55OH2eK6pOJCv7TF1BJ8LpSSTZ7sBVRhUnFqC0Aa22JqDV77k+nCDmFxSks5clz5mkKNxKTsdwGYTVIKVJBVxIAFGG3COowzV1BIFfCLkuak/rA6fhwVmYJiqFqq8LXNLcQ0TJU1I8CiSluHPlxMXjyuEuSE9nsSErlaZniAFTYu9+UUXPsvQJhSqspm1gVAUwSsbHSW8ni7qlKUpQlsCkWU7PcAxWEif3s5M2SihAYKJfU7ORQChuPKPSWScsPqSX5I96B+NkTjrCCNaEpOgEF/CkundPQsOEJy8TZswyzRGlKi+3hSd6gkqYD/kNoKzO7KkqqiYgJ5kJrRhdgSGtQcIkz8RILqmivhCqUp4n4sxNf90cfq6qiqGMVl6gqWhIJlEKoSxTp073FTHoJYLFJCWQvUk1SVKf3PpHox9SXRXEEYPGKmyyETymZwUDTZgTvf0jqu0XcLEpSVLmBDgi7EkPsA26ms/OBKp4lrQhUwpp4QBUjSkuH2Bc34naOmcStMxJcJSHO6bk6SaOSeYpUR1LEvppmfNllngzQF61IIQXQigU4BDE1q4ruxEdw2PnYdLiYlTAWYhyohQG9GPte8VqXnC1KSFFFNIAQv4U28XFqV2rtBXKs0V3hCgGJJHCh8LHyq3WJj6uH9OirTCqe1k1YM/DgFId6EAs4oFG7jlCMt7dd+QmckCvmk2qIQmYiWpCCHTMJ8KbJazDYUa2/E1G4HIJaEqSlRVMfUoCravEytr0YkU846IefNLf4E4lh/EBcpOF7xRSHLJTxUxt5Rn+QoEoJXqdRenmCIE9q8ctWICSSRLASH43J6l4TlM7xFaiSRYcSflQx7iaaVEI3Xs4vUkEHr+sFMzYoL3b7vFP7B4x9QOzi/RvrFhzfGlKSwc/bRDRRnnaPJ9Wo1JPqG+kUHM8D3XiZSer/ADpGr4iSpQBJZRLkDhtzimdqdJdDlxsbeUUmJoGZN2xmS/AsAo4fvFxwHarCzAEnwmvvf1jJ8VLINobQSI5M3g4sm+n9ilJm5ypQUP8A7H1WHLeJuDlolodR4HnZ/WMWwOYzUVTMUDsQTBJXarEsXmE82HKOCfwufytDUkanmeRSsWl9ZYMxTQlRU568OQaBuJ7MrkgkKJAJIFK1c8rgeKM2k9scSj4V/ZvDyu3WKIZSyQzNAvh+ZKrVA5IvQwS1yyyxLY7D4QblzyesNZNlxlTEqJKwhDFz4iQSSeYIZxzSYpS+2+ILu1bx6X21xFDSn6M8L+n56q0LVmoJzIaCkg1bazUoKmp+RiHgsKlCFJCiA58QvvQjixFdxFIkduZ1yEku9oRM7WTVqYgHelOHDoPSIXw7N1orl7l9mzigOJ1wQwDkvuejsHuXoaxAzbEa5KpUsBc5CSogs4qkXAv6b7AwBl9plgMUBjs1OHn5wUyftDhpSipQKVEBw3Dg2x+bREvEy49uN/4JuxuTgpwlmYpal92S6Ug6XSwZzs1SeDw1OViFaFy06nsVKVQVLhDAWHWoizye0spRCRPl6VWDezH52rSJ2Kkp+MkBKXU7VdnNtuUYvLKL3EOP0YDynCzn8a0kmvEJaoFQ23OsEZWWTRMMxR8NAA9zU7ffR4XgnJGkh6FJOwYksInYTMCVlLg8yRSuwFGjBzbbZSSog4HVrVqIam/xEitfPaOzsCs/3S+q3IDmxa/yhnOpxlJ1BKXegYeKjskO5PrFYzbtDitBShACfzO+pJPAoIbqI3hGeRJWK0gpmkjVVRSVigVuKGjg2rvAEdn5ywrVO1AJGgj8wJYg9C1YBSu0M1RZSXDgsnVYXuSbc4uOS5klSmcECxN62CrEbcufDWWPLhQk02JybJEodK31fmSSdIL/AJSGj0PydacQtcska6prZn1eda8iOEejCUrdyZSRUsywRK0kHUpQKdR5hyWFhpBD/wC6HFZctUsq75QUlJUlLbEgzEit6v5GCEuYVOsgaWPhDXA+tOVYi4tUyWlE+SCNJcvQADh5EuBYAx3rJLUTE7icgCVrMoLDaKmhUWYsUm13PygngZVO4JqGKCwKjUFV6734GHZOIWogeEFnYniQFMaj4XY8hxiNN7pxPSpYA1JUQx1OttP/AFI9IwlklPTKaS6Cipk4IUAUONwm++9xX7MM5ZOUlZmLnhSvhSiyQ/xU/KbO+8RcRj+6SoLJADJCywccHFbgClaQOweJ1l0LI1NqZSq6aPXxObX9IiONuLK5UBe0sk/1K1ENV2iHImlxpof1Z/lBztTJBUlaPECC5H/I0PMW9IJdiuw68QrvJjhF+ZD/AH7x7/jPljiS2PdkZ09KtYFFOz2IHifkzEecWbH9pkpQwPjtW3WLPmeWSJMpwEpCUsKWFm9oxLNsUJs06VUcgHbn6x0NEplrn4uh8dKFSt60ZPAXIikZ9j++nFQoH47Q+QpQCTuG8/1r7QPxWDCA4+6P8ozUaLsi4liWbdosuC7NBaQCNn9oq0hytIFaiNrynC93JTv4R8n+sUIyDEyDLJSRR4hzUKvVo1DH4KWpR1MwFvOx++MVHG5TdT+H0iUxlUVHAYLHAPt9+UMqwwHXhFCEYTBFexgzIyFRD6WHOJGR6QHSu9xRxxobxYpqiUfE9L/xEtjKnOytKR4ix4RNyrLgRdqu+/L75Q7iAHOrpBTs6PHopRnfhtBYUP4fIEq0uVO/C/SF5p2UCkjQAl9+lotP9UE1Z4fShazqDefCDkFGTK7LTdQahNjBXAZ/OlNLxA1odgelnP6xd8dggoEu7Dhvy++EVzHS9XgmIK0mymDj3jPLhhmjUkLaC+TErSXKSmrlIoomp08g7c7mI+MzBWHmIQZZUFUCuZNmsK8+kCcrmf0swJ1PJJZjs/D9YLZjhBOIDlSUkl3oDtWx/Yx4WXx3hy1Lotu1oXisdLmoCiRpJdRVqSQE3dlAkHhY0gNjMWlvAkFANQ35eIIqQxB8og9q8HLll0l1FiRq8LAjYXJr6bwFw+YrQxCLGgDhgWam37xtjwco8o/uRKw7LyJQUCli5OlTMpiS4pRq+o5xM/oEJAWi2gFKVNezHfcPzBiBhc1VOJAZC01YByWerOBR6xIzCatTswUWcGnU8qk+kZy9TlUmGhnBdo1vUJDuSogsTZ92j0VeTiTKUSS2kkEBxexDc2pHo6J+Gm7URqTLPiUsEqAmkFklIAB/KCzkB/7f/tATLZk9U4YeXqSlRqFWSCD4uFE15nnF8Od4YghSgACSk3KVEEAgFjTV0oIhrW4IZKSCoJUoElSFFCiRvcPwpGUc3GNOP+w47AWcZdiJcyXKfvHJcktYqCQSLnQHfj0heFkKlFQMs/3CWGqgYUqbEguAxtB3F5kVMpICmLFhuzA8hy8yaQ9l+EQtOlf5lAhVPGkpHA08T8N4n13x/uWhNW9FVnImKdMxSQnhRyXADE7fNobxiu4lo0qKkksqj2LFXI/XpB3OMn/uhQSSAlQSqtHBa5q31oKVhYjIZ6pSUygAf8SfC2lRCg97nz842hkg+O1X0Ip3QvKsxE2WsKQVS6gqb4Sw1EtvYve92i/9iJvdSQgBgl/F/k528mjKP9FxchfdSyRr061A+Gxo3EVryi89ksQdK0LJ1Ask9Ke8dfiyhDJUWqY2I7fdplTXkI+EFlNuXoOY/SM7l6g9CRQ1Fam/KNsyDstImTO9uXev0+94L5x2HkLlL0pAURePRYIw1Chtsx9IZxiNQbh8uMScTgTKmmWfyk+xLwgsSXa7fL7vvCQ2I7L5J3k8H8qSHrxsekapmc3u5DvYN7RW+xcgAqUGYUHr915Q72vzB0FPCnr9v/EAAaXjn16jep6FoiY9YAUBXhAqTNJt59G94mGakpFfLeIoogIn6Vu2zdKXEex2HSRqDG1rw+ilw4B9Hjk8MCAykmtNukMAWl0lgLt1glLxqwA6iNm2+/0gdMlkt99IkIBb94dCO4nEFR3doOdntTuBcb/r93gBLlsU/bPBRWOKGAcffyhUBbsDOGoKU5L0rQV4RZ5WJ1C7J3YRQsrmKUoF6NcHjFkXiiABqoa15Rm0WgtiVIZqMebfKBOZ4fUltJA4vT6iIOOzeyVAHlT6H5xPy6eVpICgBsDfpZouJLBeNypKpejiHSefCm8DcJjRLQUzFKCpbgFwwa3meNYtOIwyjJOgEqQaHetSOnK0UbMcNNK5soCsxqsxALOdtox8vGp49k3RDxOf94T/AG0eH8xCSeALs7/rAmfOCxqWHrUp24OKUPEGJ6sq7iUVTKuRZqmtADeu8B1zq6mCeVCSOb7dfSM8Sh8nQrC+KzCWSlUpWlaavpYu3He196wXk41M5LGhbjv0a0Uiaxqm3A7ftziVgUDWnW+l2UWt1/eJyeNHjp9DCua5YUEKJ1A0dncAFg3pHodxOMQCZSlnw0CixBD6g7it28hHoiMp1v8A4SDsbmBmaQAzBvasGMDngKQJkoEpFNO5Ds9WBZ6hrCKsDV4LYRKnSyjpq7m1fb+Y0yYo8aoLoO4DOySEgBqhTJLBuvNQDtsYRLzOZKWFKqEq8QCnp4qOKW2r9YFY7MFJ1J7vUAAkq6gPagcxEUV6UMD4izCz3ZvP2jFeNHugs0DA9o5KilIX4bHUa000rQwfwmYhfiGmhYHYMKhxwf3jLZGHOvTLFUqSonkSEpD8w6jFlWhYlHu1sTMVqIAbWSyuTCgGxJVsI58nhxStM0U3QTzCYEnwq7xaaEAAAEm5uKAkgfLeNh1FyqqWdyXuwFrtvXgYrechYlpClu/5QLKDGyQAPLnyh7B9oFKCUzEaEEaXY1ZhQ0Ltv5xC8eSjyjsi7NX7HZnYPX5248ovaMQFJjEsqxqZIBC/Al2JIKgwSwJ35HcNGlZLnKZsrUGJKdi8et42f1Y77QVRk/auYn+qXpFll+F1BvVq/wC6BMmWlI5n2tw8xtQQvOZxOIXcHUX/APIv+kSMAQpQcM7ONnNWI9bfpHUMtGQYRk6ndwHpyP1gNncsqOkV248SCOoEXLI5CQkAUo1Dt5cIGZ7lKnOkMQUENSgL+1flAIz1OEUnkzmvD+DECZMrVNf4EXfG4PxA7MKm3iKgx9A/QQDx+XhTqAoBZqggn/8ATeUJodgbDYspcvc7w4nEUtXkW5x6Zl5Culz6UpCxlattn/aFQ7I654JfTDwSttkh7mJsnLt2oG+RLHyAgtlORqKmVbV6F3Jbo/vBQFel4Rq1IcORzLeULzPDKQUhYOo352ZvWNOwXZSUVeEsC9Db4qH0V78orf4jZYqX3a28Jd6WYt7wBaBvZ8sxBDcDE3E4jSpqgV+vGKrgsUUrAejj7MGkTypgpyWoRYRLQ0S5q1BOq7joz9YI5BPqxYhmNGf6QGmzVKlsBTfy4CLJ2Sw40JIJB3sQeRECGXLs/gUlJHNxA3tpk+qUZkpP9xIcWuNi94sWUJYE2YQ7mCNSW4xbSapmbR8/TMZ3wKVgp00JANCTxr6UteAWPwS0O4J0t4moUqfQXtsR5cosnb/CzMPie71KTKUNQAJY1Y0gRKnmWlUpZJkqDBQqUuxLcnFU8nDGOWMfTev5/PoNAVKmgjhsUpLaUpZVAWaooxbev1ji8rKVDUXQQTrSHDAOSPKHsLlalApSzkEtwI+E1s4+R5RWSUGtgF9MshKwe5mAaVKYLB/5JJv+m8ehGDyMqSF94ylFyCNq8+NfOPRx+rBav9hFYiXhp7Mkmm9/KkWPtPkCpCZQNUBJZTXKpi1Vbk0V5SQNo9H0+SBqi1T5aCgklIISCKh9I1FuoUAz8BDM8y9aSTp0pUVBmqoJDjmz+sVcUhM2cTv1jH/zfcGrLRhcTKSVKQtJWVpIDtq0ABIb8oDqIJ/xHKLDgZiNCELp3qlMkf5JSlQrs7G/+UZvJG8SkY6YGZR8JChWxFARwLQ5eKmuxrRc5uDQpCgyVHZ/8gCPnRoA5vlSJYSpBSFs+kHUk0cadVqs70Z4DTcZMV+Y9LVNSesMlXFwYzw+JKD3L8AHMuw8+aBpEpJs5VcMWBCX+xFv7L/1WFWBNR4HJJSQQPSreUZsmYoVBPrEuRm09Npizxcv846FhUXaDRdu0OXJXO72WGCw9vukT8h7PWJvwiXkE8TpSaVYe/CLXk+FGoD06bRbGmE+zmTgVUAR97RGzLAICiApjVgbNRw8WvCS2RGD/ib2mnSsepEtfhAZSWpX9vmYlLYMvy8uSt6APQ/fvEebkEtXBjZqdR98IyiV20xQZl7/ALRzE9s8WSohbOXDbF7/AHxMWmTRpU3s/JZwQAbjow+VIbmYbCoSpRY0P0f5fbxkM7O8Qtwqautbtw/SF4fM5oQpIWpr33Yj5QSutAkaKifJBJCS1j5u3zvDwxoskM5BILWevqKecZdh8wmILhRPUvFxyPN0zALCYKtxpa9njCfNG0VBlpw+azUkLb4XoKvqYP6V9IOJwqMfhhLWa0POgiuYfGaRzPN3SUgj9mh7Jsz7qY6TRXszv0q3pBjm+mTKH0Kl2n7GTsK62dHEC1d472axAAAKb0f7rG0JMrFySksXFvf9DGY4ns6vCzikh0EuD67xrRKY9/piJhBFRflUQfyjJkpLikRcDhWTv98YMZbNanCF0MOyE6UPeJeHZQcfYvEfCEKSQY5hMQx02a8O0xUZ5+MeVpMtM5yCgs7O2r+IynDyRpZM0KSSxBSoV5Fi3WPoLtphEz5C5anIUK/YjGcb+HuLQQUATEk/lLKA4lKm9iYidDSdEjIFoky9KVaz+YBlbmw+IUvTf1XNSD3hladRNN2J8Dk2uIHTOzmLCxrkaJalADUsUZyD4VOVM5fjFuy8pSpMk7gkk/mIYBzxJPlHFkwrlyvZccd/YrCsQpctBWyWNSNqG441FI7FgzCUladJ1AcQSCPOPRahEn0mWLthl+rBskDwpBI3oHvvGPYxewjYsfjpq5UwOkBSVBNAwPirWpDNarDeMqxGSTPGoioqG38QBvXdJ6GOrHkVUOUfcDlZjyY6EbV+w8c7uNbMxyXCwKGG3hxKx+sVYhSEQ4UWjyVgCErxCeZh0gs8ECzQ4ExHXiISicTC0M0LsJjG0P8ADq0n5j5xsOW5eAQRGB9lpngmMbEH5/pH0d2VnCbh5S2qpIPtCYl2Eghkx8vfiir/AORneXyj6nmppHyx+KksjMpuog2Zg1IlDK5ppHm+cc1Q9JS+rgEk+rJHuoQ2MjrrHlFqCPPCGhNAcf2PsfsQ7InqlkKTQg0hvTCgh4dCLpk2bCelIIGpI0mtwLEdAw8oKypTED67RRcixIkzQs1A2c+7Vi/4HNcLOAVRJ3sDRJevCjOXNY5Zx4vRtGVoP5JipktQ01HxeXCLahcnFJAVc+oioZSZbEpeoIGrZq3F7+0TlTg4KQygRbg3vDjm9mDx+6Cs7JTLqPEmGU4bSaBobldpjJOmb4kWKudG9osOW4iRiUky1B+vT9Y21JaM9xexnLUOQfaI+dvJXqbwq386wV/pCjavziWiWmdLKVgdNwREQVaZUt7RTM0xRUihpyjkolSQSDbY1pFuRkspIYJoYCZwO6BIHh4c4WSOrKhLdFK7WLZcoqZi4IG1QUqO4sR5wKkzEjEKF0pQ78dTt8jE/OZ/9QoqIBCWqLvQGrXdNuI84i4OWGWdOwAdwfU0s1Wa8YspsampBL7bNHom4cJQ+t0b1D8uUehUaWD8H3igy2dIB0mjKAANbB6X4QvFJLK8LswG76iHt0ANPOH14RThTsC//YuC/Qn5Q5hcOsKIcMpRJB4MXZ+ghUY2wJjMplzAdKAklqAG41XI8w/N4Df6GQJwUliA4fmqW3p4ouy8KBR0AkixLl4nzMvTpclyKUJN/V/rFqTQ6TMWXLI2rCBGs5l2ZwsxKiQUKd9Qvu9GZnekVzG9i5t5RQphUBxTzevnHRGaZm40Ukkx6LKezs1IV3qVIb4SzpNFG42B0j/tAhtiKxoqZBBUI60TUIcFtgVeQFTWG0lNA7PBoAr2QmstaD+ZLjyP7xv34X4zVhQjdClD3cexjHcjyFSZf9QPEgOlwKOpjdy9BF//AAonr76ch/BpB5O7e4HtB8ovc1nU4NGvw9aR8zfi8xx6m+X13j6V10MYX28wScTPWolgCACkpuOPCh4xHNLsviZrgcOVy5m2nQSeAdTk/JuJELm4Qy5awpJBUoBt2Syq8aqTbdMXHLsilEKLu+lKkUFi3vUxJzDKZKypSlB3T+ZN/E4SDUs5OmI9VWCRm6ZTpJ2DDqS/0BPlCSgi8XwZLKSRbch06h4nuLEACnXziLjeyCygKSpBYD83iPhSNwxN+FGvFLLFug4lNMdK4LY3I5yFFOhR5gEjc8OEClIjRST6JaoQFGFomtaONyjwg0BYsl7RrSWWshLUL2NnZmMWXKe0iXTrPxDSCbAh26O49ozlVI5pJ3jOWKLLU2jUsJmkqeCkABPhTX/I0XR6MPFEbDYubg52qWSCC7GxHMRnmGnKTQH74xc8PMVNRLUq5SPa3tELG4vQ3O+zc8hzVGMkBYorccDvHZmtCgRuWPnFE/DvFmXiNFWWLcxvGoT8O4jTsno9JW6fL7+UUPtfitC9P5VV6FNG/iLZ3+hPRTesUTttNUtWlCbXNPO7daEQZP0ih+oDKSkyXCvEFM4LU0gg/wDkk9YZTIASoPXSySa9Cz8TblA6TilfCUTEtQjwpehBIBNeTk+8T5EpeySlwankLhhudo5KNqOKSVjSoFI0tQDjSn/WOx3E4FUxGlSgKM5pY9WL9eEegSHVEzuUJSylcWcjYh99oc/0+WovoDjcAb/OGZMmYkkFT1pz+j+nTg9IRMY6mF2YUarUJvDD8DsxBSP8eekEeZh4yw9eNLD5msR9S6eEHh4h5u3X7aJMlTpqa2IB+RP7QAhvG4d0kKKQdnVLN7FiSCfWFailJ8IUoWZSf18/KPBv5H2IUq1h99YAGUlRDhBN/Cliel2gFipEsznXhlAbrUDc+Eg6fhoSK8YPhDbWPAfWPawWNP0/T9oE6E1aKgvstKUFd0Vo1JbxCjOFHmxYDo/GCvZX8ITP1LxE4ol2R3bFSm3OoMB5bQbmSwRc/p90idl+NnSxKQhZ0yi6Bd3ulX+XLcRpHLXZLh9A9P7PycBlU2Qk6glCiFLAcqNQSzVctSBv4P4JsMucq8xZ22R4R7v6way/MpWOR4whSQogoopJKSxcG9duUG8KqWn+2gJSEsNKQABRxQWjVz0Qons3maZKzYBJrGO4jLTNWCZqyLkMyS+xcfKrNdo0XtfmIKDKSauNQDWu31imEcA/MtGGSRrFA5eWeLUFadQZR4sHHy9vRUvCgKUxUeOrS1BtTl70iZMqDz97ddoUk8B7m1DGbKSIWGwZV4VhLH8wcfQ8Y7icr4JC0ONV0l3cV9OsTlA/lFOZP6GHROKXGpgbh2f5Qh0gdKlIq8sO25f1cmGFZHhpnxSUPyAHXhWCoQGoRHkS0itTwFWvzgTBorOM7FSFOUBaP+Jcf+zwOm/h/RxNVzdFhbaLzMmgj8tAHvfz35dIbSkmgrtS/wA7s8WskiXFFAxP4fTAHRNQrkQR8n94BY7IMRJDrlKCbOGI9iW82jWVMDcXqSD50bnDWMVOCgqUUOkuQpLAinJx6CLjll7icEUnIfw4zHFJStMjRLU3jmKCC3EJPi9RWL9257Ny8H3Jl0SU6SOaQK+cWk9sQBKOgirLRRwSNi+kgHnURXO2GKXmWIw+HlKSlAdS1KNQCwom6jRhYP0MbLImzJwaBf4fzFKzAJbwJllRLbuBT1jaVqGmsAsl7P4fDpQEglQHxKLqIF3sPQCJOe5iiVLZRbVQffCFyodWVXNc5BQpjV08Ob/OK0oFSioqUbuDpb2D7w/OKeAU9mY+m8JVNUwCQW4OUg/8mH3yjnlNyNoxoQocABz019q+kckkgOCKGl+ABc84aUti7Gzku/8AMKKqDfp05mJGMTJYrqepozqGnYNtVz/EeiVL1f4sOfQcCXj0FhRxRchn9/3JjpJG6qeUPoSDv8/u8e0c343+/wCIokZCOZ8/4eO94bv6nrxEPBRZiH89uUeSs3KW9+sIZ5CTvqA5NXjvSOlLks7UvtHJxcUJTzADu/MEb8IbJU99vXi7M1oAHmAu/GphYlA7K8q8HaGJIJu56K8t4WwGxd+NqgG4v+kOmB4lPTlvyvC0FD1B1cXFvscYjz5jGgobEXJpSp3hcogO5B6J6cxy9IKAdwbS1mZL8K+Iars8KXiV6jM1qCyKlKm1kNQswPI1iNNUhNXU5vQHnSojmhLA6qWYhrvdn+sFMCQqapR1GpLubvwq3D5QgTHelOf28RZlHAu1Dt6NEhU4MBXgOHpCoB0qSQHI6W+f3SG0igcv+tvn5RHMxZa1eZp7Wh2WLEnkT9iChiyDckt5tTixhuYBwq4rX33O1ucKIA2oRx822joCRtv9YKEMhX+2uxIFQ1nf+IdB30gb1cW844tYt98obALsAPvrs+36wAKGslgoh7kKFG4D7vCVggGr/wC6pIfhc+0KKaUuRz8r04GI5lEUJBO93pfeHQC5ZANC9SCX+h89t453iVgAgqJP+I6OSzg0hRFwa9P3hPdj4iXagJHmNoAHiEqAcAtSh+gtDU/K5IUmc7TQaKSS7dd/P6QoAO4PI39xv+8LIFA21LC3tzgQE7E5tOUUqM0umxSwPMHYjkRDGNxs2ZVRUo8aCnKjfxEdZbjT74w2tKqVtTh1tyh9hQsTyku6kpHMkDpQD+YaVMBBJJCTUsVfR44QogNSv8/X2hxSNThVQaNtTkfughUFjZx4slwSP9p4NQBxEk4olywccEi/n+sRgQ5B68Y6lCiHcH2tY8fKCrCx+dPTV6P/AMQ3l+8eiPMNwoFXKnyNI9BxCz//2Q==" alt="Avatar">
						        </div>
						        <div class="contentv ">
						            <div class="vinte">
						                <div>Nome do pet: ${aprovacao.pet.nome}</div>
						                <div>Idade: ${aprovacao.pet.idade}</div>
						                <div>Tamanho: ${aprovacao.pet.tamanho}</div>
						                <div>Ra�a: ${aprovacao.pet.raca.nome}</div>
						                <div>Categoria: ${aprovacao.pet.tipo.nome}</div>
						                <div>Endereco: ${aprovacao.pet.usuario.cidade}/${aprovacao.pet.usuario.estado}</div>
						                <div>Nome do Dono: ${aprovacao.pet.usuario.nome}</div>
						                <div>Contato: ${aprovacao.pet.usuario.telefone}</div>
						                <c:choose>
						                	<c:when test="${aprovacao.pet.venda==true}">
						                		<div>Valor: ${aprovacao.pet.valor}</div>
						                	</c:when>
						                	<c:otherwise>
						                		<div>Doa��o</div>
						                	</c:otherwise>
						                </c:choose>
						            </div>
						            <div class="cinquenta">
						                <div>Descri��o: ${aprovacao.pet.descricao}</div>
						                <div>Observa��o: ${aprovacao.pet.observacao}</div>
						                <c:if test = "${aprovacao.status == '2'}">
						                	<div style="color:red">Reprovado: ${aprovacao.msg}</div>
						                </c:if>
						            </div>
						            <div style="float: left">
						           	
						            </div>
						        </div>
						        	<input type="button" class="btn" value="Alterar" onClick="location.href='/Consultorio/pet/editar?petId=${aprovacao.pet.id}'"/>
						        	<input type="button" class="btn" value="Remover" onClick="location.href='/Consultorio/pet/remover?petId=${aprovacao.pet.id}'"/>
								<c:if test = "${aprovacao.status == '1'}">
									<c:choose>
									    <c:when test="${aprovacao.pet.disponivel==true}">
									    	<input type="button" class="btn" value="Colocar Disponivel" onClick="location.href='/Consultorio/pet/disponibilidade?petId=${aprovacao.pet.id}'"/>
								        </c:when>
								        <c:otherwise>
											<input type="button" class="btn" value="Colocar Ind�sponivel" onClick="location.href='/Consultorio/pet/disponibilidade?petId=${aprovacao.pet.id}'"/>
								        </c:otherwise>
									</c:choose>
								</c:if>
									<c:choose>
									    <c:when test="${aprovacao.status == '0'}">
									    	<input type="button" class="btn" disabled value="Esperando aprova��o"/>
								        </c:when>
								        <c:when test="${aprovacao.status == '1'}">
									    	<input type="button" class="btn btn-success" disabled value="Aprovado"/>
								        </c:when>
								        <c:otherwise>
											<input type="button" class="btn" value="Enviar para aprova��o" onClick="location.href='/Consultorio/pet/enviarParaParaAprovacao?petId=${aprovacao.pet.id}'"/>
								        </c:otherwise>
									</c:choose>
						    </div>
				        </c:forEach>
						</div>
					</div>
				</div>
			</c:when>
	        <c:otherwise>
	        	<div class="row" style="padding-top:30px">
		      		<div class="col-lg-12 text-center">
						<h3 class="mt-5">Nenhuma pet cadastrado!</h3>
					</div>
				</div>
	        </c:otherwise>
		</c:choose>
			<%-- <c:forEach var="pet" items="${pets}" >
				
	        </c:forEach> --%>
	</div>
</body>
</html>