package consultorio.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="tipo")
public class Tipo {

	@Id
	@Column(name="id")
	@GeneratedValue
	private long id;
	
	@Column(name="nome")
	private String nome;
	
	@Cascade(CascadeType.DELETE)
	@OneToMany(fetch = FetchType.EAGER, mappedBy="tipo")
	private List<Raca> racas;
	
	@Cascade(CascadeType.DELETE)
	@OneToMany(fetch = FetchType.EAGER, mappedBy="tipo")
	private List<Pet> pet;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Raca> getRacas() {
		return racas;
	}

	public void setRacas(List<Raca> racas) {
		this.racas = racas;
	}
	
}
