package consultorio.beans;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="userdetail")
public class Userdetail implements Comparable<Userdetail> {

	@Id
	@Column(name="id")
	@GeneratedValue
	private long id;
		
	@Column(name="firstname")
	private String firstname;
	
	@Column(name="lastname")
	private String lastname;
	
	@Column(name="username")
	private String username;

	// 0 -> Undefined
	// 1 -> Ok
	// 2 -> Blocked
	@Column(name="status")
	private int status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int compareTo(Userdetail mj) {
		int ret = 0;
		if(this.id > mj.id){
			ret = 1;
		} else if(this.id < mj.id) {
			ret = -1;
		}
		return ret;
	}

}