package consultorio.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name="aprovacao")
public class Aprovacao {
	
	@Id
	@Column(name="id")
	@GeneratedValue
	private long id;
	
	@NotFound(action=NotFoundAction.IGNORE)
	@OneToOne(fetch = FetchType.EAGER)
	private Usuario usuario;
	
	@NotFound(action=NotFoundAction.IGNORE)
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Pet pet;
	
	@Column(name="msg")
	private String msg;
	
	@Column(name="status")
	private int status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Pet getPet() {
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
