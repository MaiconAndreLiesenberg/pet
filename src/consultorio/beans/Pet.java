package consultorio.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name="pet")
public class Pet {

	@Id
	@Column(name="id")
	@GeneratedValue
	private long id;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="tamanho")
	private String tamanho;
	
	@Column(name="idade")
	private String idade;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="disponivel")
	private boolean disponivel;
	
	@Column(name="venda")
	private boolean venda;
	
	@Column(name="valor")
	private String valor;
	
	@Cascade(CascadeType.DELETE)
	@OneToOne(mappedBy="pet", fetch=FetchType.EAGER)
	private Aprovacao aprovacao;
	
	@ManyToOne
	@JoinColumn(name="id_tipo")
	@NotFound(action=NotFoundAction.IGNORE)
	private Tipo tipo;
	
	@ManyToOne
	@JoinColumn(name="id_raca")
	@NotFound(action=NotFoundAction.IGNORE)
	private Raca raca;
	
	@ManyToOne
	@JoinColumn(name="id_usuario")
	@NotFound(action=NotFoundAction.IGNORE)
	private Usuario usuario;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTamanho() {
		return tamanho;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public Raca getRaca() {
		return raca;
	}

	public void setRaca(Raca raca) {
		this.raca = raca;
	}

	public boolean isDisponivel() {
		return disponivel;
	}

	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}

	public boolean isVenda() {
		return venda;
	}

	public void setVenda(boolean venda) {
		this.venda = venda;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Aprovacao getAprovacao() {
		return aprovacao;
	}

	public void setAprovacao(Aprovacao aprovacao) {
		this.aprovacao = aprovacao;
	}
	
}
