package consultorio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import consultorio.beans.Raca;

@Transactional
@Repository
public class RacaDao {

	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Raca> getRacaById(long id) {
		List<Raca> raca = null;
		try {
			Query query = manager.createQuery("select u from Raca as u where id = "+id);
			raca = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return raca;
	}
	
	@SuppressWarnings("unchecked")
	public List<Raca> getAllRacaExceptId(long id) {
		List<Raca> raca = null;
		try {
			Query query = manager.createQuery("select u from Raca as u where id <> "+id);
			raca = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return raca;
	}
	
	@SuppressWarnings("unchecked")
	public List<Raca> getAllRaca() {
		List<Raca> raca = null;
		try {
			Query query = manager.createQuery("select u from Raca as u ");
			raca = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return raca;
	}
	
	public Raca getTipoById(int id) {
		Raca raca = null;
		try {
			Query query = manager.createQuery("select u from Raca as u where u.id="+id);
			raca = (Raca) query.getResultList().get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return raca;
	}
	
	public void add(Raca raca) {
		try {
			manager.persist(raca);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void update(Raca raca) {
		try {
			manager.merge(raca);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void remove(long id) {
		try {
			Raca raca = manager.find(Raca.class, id);
			manager.remove(raca);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
