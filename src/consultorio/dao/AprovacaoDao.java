package consultorio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import consultorio.beans.Aprovacao;

@Transactional
@Repository
public class AprovacaoDao {
	
	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Aprovacao> getByIdPet(long id) {
		List<Aprovacao> aprovacao = null;
		try {
			Query query = manager.createQuery("select u from Aprovacao as u where u.pet.id = "+id);
			aprovacao = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aprovacao;
	}
	
	@SuppressWarnings("unchecked")
	public List<Aprovacao> getAllNAvaliados() {
		List<Aprovacao> aprovacao = null;
		try {
			Query query = manager.createQuery("select u from Aprovacao as u where u.status = 0");
			aprovacao = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aprovacao;
	}
	
	@SuppressWarnings("unchecked")
	public List<Aprovacao> getAllById(long id) {
		List<Aprovacao> aprovacao = null;
		try {
			Query query = manager.createQuery("select u from Aprovacao as u where u.usuario.id = "+id);
			aprovacao = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aprovacao;
	}
	
	@SuppressWarnings("unchecked")
	public List<Aprovacao> getAllAprovados() {
		List<Aprovacao> aprovacao = null;
		try {
			Query query = manager.createQuery("select u from Aprovacao as u where u.pet.disponivel = false and u.status = 1");
			aprovacao = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aprovacao;
	}

	public void add(Aprovacao aprovacao) {
		try {
			manager.persist(aprovacao);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void update(Aprovacao aprovacao) {
		try {
			manager.merge(aprovacao);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
