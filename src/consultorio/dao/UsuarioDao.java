package consultorio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import consultorio.beans.Usuario;

@Transactional
@Repository
public class UsuarioDao {

	@PersistenceContext
	EntityManager manager;

	@SuppressWarnings("unchecked")
	public List<Usuario> getAllUsuario() {
		List<Usuario> usuario = null;
		try {
			Query query = manager.createQuery("select u from Usuario as u");
			usuario = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usuario;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> getUsuarioById(long id) {
		List<Usuario> usuario = null;
		try {
			Query query = manager.createQuery("select u from Usuario as u where id = "+id);
			usuario = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usuario;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> getUsuarioPorNome(String username) {
		List<Usuario> usuario = null;
		try {
			Query query = manager.createQuery("select u from Usuario as u where username = '"+username+"'");
			usuario = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usuario;
	}
	
	public void add(Usuario usuario) {
		try {
			manager.persist(usuario);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void update(Usuario usuario) {
		try {
			manager.merge(usuario);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
}
