package consultorio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import consultorio.beans.Pet;

@Transactional
@Repository
public class PetDao {

	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Pet> getAllPetsValidos() {
		List<Pet> pet = null;
		try {
			Query query = manager.createQuery("select u from Pet as u where disponivel = false and u.Aprovacao.status = 1");
			pet = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pet;
	}
	
	@SuppressWarnings("unchecked")
	public List<Pet> getPetById(long petId) {
		List<Pet> pet = null;
		try {
			Query query = manager.createQuery("select u from Pet as u where id = "+petId);
			pet = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pet;
	}
	
	@SuppressWarnings("unchecked")
	public List<Pet> getPetByUserId(long idUsuario) {
		List<Pet> pet = null;
		try {
			Query query = manager.createQuery("select u from Pet as u where id_usuario = "+idUsuario);
			pet = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pet;
	}
	

	public void add(Pet pet) {
		try {
			manager.persist(pet);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void update(Pet pet) {
		try {
			manager.merge(pet);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	public void remover(long id) {
		try {
			Pet pet = manager.find(Pet.class, id);
			manager.remove(pet);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
