package consultorio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import consultorio.beans.Tipo;
import consultorio.beans.Usuario;

@Transactional
@Repository
public class TipoDao {

	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Tipo> getAllTipoExceptId(long id) {
		List<Tipo> tipo = null;
		try {
			Query query = manager.createQuery("select u from Tipo as u where id <> "+id);
			tipo = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tipo;
	}
	
	@SuppressWarnings("unchecked")
	public List<Tipo> getAllTipo() {
		List<Tipo> tipo = null;
		try {
			Query query = manager.createQuery("select u from Tipo as u ");
			tipo = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tipo;
	}
	
	public Tipo getTipoById(long id) {
		Tipo tipo = null;
		try {
			Query query = manager.createQuery("select u from Tipo as u where u.id="+id);
			tipo = (Tipo) query.getResultList().get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tipo;
	}
	
	
	public void remove(long id) {
		try {
			Tipo tipo = manager.find(Tipo.class, id);
			manager.remove(tipo);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void add(Tipo tipo) {
		try {
			manager.persist(tipo);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void update(Tipo tipo) {
		try {
			manager.merge(tipo);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
