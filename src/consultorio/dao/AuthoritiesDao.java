package consultorio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import consultorio.beans.Authorities;

@Transactional
@Repository
public class AuthoritiesDao {
	
	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Authorities> getByName(String username) {
		List<Authorities> tipo = null;
		try {
			Query query = manager.createQuery("select a from Authorities as a where a.username LIKE CONCAT('%',?1,'%')");// and authority = 'ROLE_FUNC'");
			query.setParameter(1, username);
			tipo = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tipo;
	}

	public void removeFunc(String username) {
		
		Authorities au = getByName(username).get(0);
		try {
			manager.remove(au);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
