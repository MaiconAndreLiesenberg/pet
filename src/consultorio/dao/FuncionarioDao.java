package consultorio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import consultorio.beans.Funcionario;

@Transactional
@Repository
public class FuncionarioDao {

	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public void removeFuncionario(long id) {
		try {
			Funcionario funcionario = getFuncionarioById(id).get(0);
			manager.remove(funcionario);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Funcionario> getAllFuncionarios() {
		List<Funcionario> funcionario = null;
		try {
			Query query = manager.createQuery("select u from Funcionario as u");
			funcionario = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return funcionario;
	}
	
	@SuppressWarnings("unchecked")
	public List<Funcionario> getFuncionarioById(long id) {
		List<Funcionario> funcionario = null;
		try {
			Query query = manager.createQuery("select u from Funcionario as u where id = "+id);
			funcionario = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return funcionario;
	}
	
	@SuppressWarnings("unchecked")
	public List<Funcionario> getAllFuncionarioSemSala() {
		List<Funcionario> funcionario = null;
		try {
			Query query = manager.createQuery("select u from Funcionario as u where jaTemSala = FALSE");
			funcionario = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return funcionario;
	}
	
	
	public void add(Funcionario funcionario) {
		try {
			manager.persist(funcionario);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
