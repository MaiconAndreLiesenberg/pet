package consultorio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import consultorio.beans.Userdetail;

@Repository
public class UserdetailDao {

	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Userdetail> getAllUserdetail() {
		List<Userdetail> userDetail = null;
		try {
			Query query = manager.createQuery("select u from Userdetail as u ");
			userDetail = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<Userdetail> getUserdetailByUsername(String username) {
		List<Userdetail> userDetail = null;
		try {
			Query query = manager.createQuery("select u from Userdetail as u where username = '"+username+"'");
			userDetail = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDetail;
	}
	
	public void add(Userdetail userdetail) {
		System.out.println("userdetailDao");
		try {
			manager.persist(userdetail);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void update(Userdetail userdetail) {
		try {
			manager.merge(userdetail);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
