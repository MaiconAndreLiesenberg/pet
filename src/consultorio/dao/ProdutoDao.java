package consultorio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import consultorio.beans.Produto;

public class ProdutoDao {

	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Produto> getProdutoById(long id) {
		List<Produto> produto = null;
		try {
			Query query = manager.createQuery("select u from Produto as u where id = "+id);
			produto = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return produto;
	}
	
	@SuppressWarnings("unchecked")
	public List<Produto> getAllProduto() {
		List<Produto> raca = null;
		try {
			Query query = manager.createQuery("select u from Produto as u ");
			raca = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return raca;
	}
	
	public void add(Produto produto) {
		try {
			manager.persist(produto);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void update(Produto produto) {
		try {
			manager.merge(produto);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void remove(long id) {
		try {
			Produto produto = manager.find(Produto.class, id);
			manager.remove(produto);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
