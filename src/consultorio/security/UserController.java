package consultorio.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import consultorio.beans.Userdetail;
import consultorio.beans.Usuario;
import consultorio.dao.UserdetailDao;
import consultorio.dao.UsuarioDao;

@SuppressWarnings("deprecation")
@Controller
@Transactional
public class UserController {

	private UserBeam userbeam;
	
	@Autowired
	private UserdetailDao userdetailDao;
	
	@Autowired
	private UsuarioDao usuarioDao;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private JdbcUserDetailsManager jdbcUserDetailsManager;
	
	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@RequestMapping("cadastroUsuario")
	public ModelAndView newUserAdd(Usuario usuario, String username, String password) {

		userbeam = new UserBeam();
		
		System.out.println("cidade: "+usuario.getCidade());
		
		List<Usuario> usuarioList = null;
		try {
			usuarioList = usuarioDao.getUsuarioPorNome(username);
		} catch (Exception e) {}
		
		ModelAndView model = new ModelAndView();
		
		if ( usuarioList == null || usuarioList.size() == 0 ) {
			
			List<String> grantedAuthorities = new ArrayList<String>(); 
			grantedAuthorities.add("ROLE_USER");

			userbeam.setUsername(username);
			userbeam.setPassword(password);
			userbeam.setGrantedAuthoritiesByName(grantedAuthorities);
			userbeam.setPasswordEncoder(passwordEncoder);
			userbeam.setUserDetailsManager(jdbcUserDetailsManager);
			userbeam.setEnabled(true);
			userbeam.createUser();
			
			usuarioDao.add(usuario);

		}else {
			
			model.addObject("erro", "Este usuario j� existe!");
			model.addObject("email", usuario.getEmail());
			model.addObject("endereco", usuario.getEndereco());
			model.addObject("nome", usuario.getNome());

		}
		
		model.setViewName("login");
		return model;
		
	}
	
	@RequestMapping("isAdmin")
	public boolean getRole(HttpServletRequest request) {  
		return request.isUserInRole("ROLE_ADMIN"); 
	}
	
}
