package consultorio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import consultorio.beans.Pet;
import consultorio.beans.Usuario;
import consultorio.dao.UsuarioDao;
import consultorio.security.UserBeam;

@Controller
public class UsuarioController {
	
	@Autowired
	private UsuarioDao usuarioDao;

	@RequestMapping(value = "/perfil", method = RequestMethod.GET)
	public ModelAndView perfil(HttpServletRequest request) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String username = user.getUsername();
	    
	    Usuario usuario = usuarioDao.getUsuarioPorNome(username).get(0);
	    
	    System.out.println("user: "+username);
	    
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("usuario", usuario);
		model.setViewName("meuperfil");

		return model;

	}
	
	@RequestMapping(value = "/perfil/editarPerfil", method = RequestMethod.POST)
	public ModelAndView perfilEdita(HttpServletRequest request, Usuario usuario) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
	    String username = user.getUsername();
	    
	    Usuario usuario2 = usuarioDao.getUsuarioPorNome(username).get(0);
	    
	    usuario2.setNome(usuario.getNome());
	    usuario2.setEmail(usuario.getEmail());
	    usuario2.setEndereco(usuario.getEndereco());
	    
	    usuarioDao.update(usuario2);
	    
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("usuario", usuario2);
		model.addObject("msg", "Dados atualizados!");
		model.setViewName("meuperfil");

		return model;

	}
	
	@RequestMapping(value = "/perfil/alteraSenha", method = RequestMethod.GET)
	public ModelAndView alteraSenha(HttpServletRequest request) {
	    
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		
		model.setViewName("senha");

		return model;

	}
	
	@RequestMapping(value = "/perfil/trocarSenha", method = RequestMethod.POST)
	public ModelAndView alteraSenha(HttpServletRequest request, String antiga, String nova) {
	    
		System.out.println("antiga: "+antiga);
		System.out.println("nova: "+nova);
		
		
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		
		model.setViewName("senha");

		return model;

	}
}
