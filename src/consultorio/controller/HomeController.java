package consultorio.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import consultorio.beans.Aprovacao;
import consultorio.beans.Funcionario;
import consultorio.beans.Pet;
import consultorio.dao.AprovacaoDao;
import consultorio.dao.FuncionarioDao;
import consultorio.dao.PetDao;

@Controller
public class HomeController {
	
	@Autowired
	private PetDao petDao;
	
	@Autowired
	private FuncionarioDao fDao;
	
	@Autowired
	private AprovacaoDao aDao;

	@RequestMapping(value = {"/","/home" }, method = RequestMethod.GET)
	public ModelAndView homePage(HttpServletRequest request) {
		
		List<Aprovacao> aprovados = aDao.getAllAprovados();
		
		List<Pet> pets = new ArrayList<>();
		
		for (Aprovacao aprovacao : aprovados) {
			pets.add(aprovacao.getPet());
		}
		
//		List<Pet> pets = petDao.getAllPetsValidos();
		
		List<Funcionario> f = fDao.getAllFuncionarios();
		for (Funcionario funcionario : f) {
			System.out.println("nome: "+funcionario.getUsuario().getNome());
		}
		
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("pets", pets);
		model.setViewName("index");
		return model;

	}

	@RequestMapping(value = {"/login"}, method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "login_error", required = false) String login_error,
			@RequestParam(value = "logout", required = false) String logout) {
		System.out.println("error: "+login_error);
		ModelAndView model = new ModelAndView();
		if (login_error != null) {
			model.addObject("error", "Inv�lido username ou senha!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}

}