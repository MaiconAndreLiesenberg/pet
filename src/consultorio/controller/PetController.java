package consultorio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import consultorio.beans.Aprovacao;
import consultorio.beans.Pet;
import consultorio.beans.Raca;
import consultorio.beans.Tipo;
import consultorio.beans.Usuario;
import consultorio.dao.AprovacaoDao;
import consultorio.dao.PetDao;
import consultorio.dao.RacaDao;
import consultorio.dao.TipoDao;
import consultorio.dao.UsuarioDao;

@Controller
public class PetController {
	
	@Autowired
	private TipoDao tipoDao;
	
	@Autowired
	private RacaDao racaDao;
	
	@Autowired
	private PetDao petDao;
	
	@Autowired
	private AprovacaoDao aproDao;

	@Autowired
	private UsuarioDao usuarioDao;
	
	@RequestMapping(value = "/pet", method = RequestMethod.GET)
	public ModelAndView pet(HttpServletRequest request) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String username = user.getUsername();
	    
	    Usuario usuario = usuarioDao.getUsuarioPorNome(username).get(0);
	    
	    List<Aprovacao> aprovacao = aproDao.getAllById(usuario.getId());
	    
	    System.out.println("tamanho: "+aprovacao.size());
	    
	    for (Aprovacao aprovacao2 : aprovacao) {
			System.out.println("pet nome: "+aprovacao2.getPet().getNome());
		}
	    
//	    List<Pet> pets = petDao.getPetByUserId(usuario.getId());
	    
	    System.out.println("user: "+username);
	    
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("aprovacoes", aprovacao);
		model.setViewName("pet");

		return model;

	}
	
	@RequestMapping(value = "/pet/cadastro", method = RequestMethod.GET)
	public ModelAndView petCadastro(HttpServletRequest request) {

		List<Tipo> tipos = tipoDao.getAllTipo();
		List<Raca> racas = racaDao.getAllRaca();
		
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("tipos", tipos);
		model.addObject("racas", racas);
		
		model.setViewName("petCadastro");

		return model;

	}
	
	@RequestMapping(value = "/pet/enviarParaParaAprovacao", method = RequestMethod.GET)
	public String enviarAprovacao(HttpServletRequest request, @RequestParam(value = "petId",  required = true) int petId) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String username = user.getUsername();
		
	    System.out.println("userCadastro: "+username);
	    
	    Usuario usuario = usuarioDao.getUsuarioPorNome(username).get(0);
	    
	    Aprovacao apro = aproDao.getByIdPet(petId).get(0);
	    if(apro.getUsuario().getId() == usuario.getId()) {
	    	apro.setStatus(0);
	    	apro.setMsg("");
	    	aproDao.update(apro);
	    }
		
	    return "redirect:/pet";

	}
	
	@RequestMapping(value = "/pet/cadastrarPet", method = RequestMethod.POST)
	public String cadastrarPet(HttpServletRequest request, Pet pet, int tipoId, int racaId, int vendaForm) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String username = user.getUsername();
		
	    System.out.println("userCadastro: "+username);
	    
	    Usuario usuario = usuarioDao.getUsuarioPorNome(username).get(0);
	    
		pet.setTipo(tipoDao.getTipoById(tipoId));
		pet.setRaca(racaDao.getTipoById(racaId));
		pet.setUsuario(usuario);
		
		if(vendaForm == 1) {
			pet.setVenda(true);
			pet.setValor(pet.getValor());
		}
		
		Aprovacao aprovacao = new Aprovacao();
		aprovacao.setPet(pet);
		aprovacao.setUsuario(usuario);
		aprovacao.setStatus(0);
		
		aproDao.add(aprovacao);
		
//		petDao.add(pet);
		
		return "redirect:/pet";

	}
	
	@RequestMapping(value = "/pet/editar", method = RequestMethod.GET)
	public ModelAndView editarPet(HttpServletRequest request, @RequestParam(value = "petId",  required = true) int petId) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String username = user.getUsername();
		
	    System.out.println("userCadastro: "+username);
	    
	    Usuario usuario = usuarioDao.getUsuarioPorNome(username).get(0);
	    
	    Pet pet = petDao.getPetById(petId).get(0);
	    
	    System.out.println("pet: "+pet.getNome());
	    
	    ModelAndView model = new ModelAndView();
	    model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
	    model.addObject("func", request.isUserInRole("ROLE_FUNC"));
	    model.addObject("user", request.isUserInRole("ROLE_USER"));
	    
	    List<Tipo> tipos = tipoDao.getAllTipoExceptId(pet.getTipo().getId());
	    List<Raca> racas = racaDao.getAllRacaExceptId(pet.getRaca().getId());
	 
	    if(pet.getUsuario().getId() == usuario.getId()) {
	    	   model.addObject("pet2", pet);
	    	   model.addObject("tipos2", tipos);
	    	   model.addObject("racas", racas);
	    	   model.setViewName("editarPet");
	    }else {
	    	model.setViewName("pet");
	    }

		return model;

	}
	
	@RequestMapping(value = "/pet/editarPet", method = RequestMethod.POST)
	public String alterarPet(HttpServletRequest request,  Pet pet, int tipoId, int racaId) {
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String username = user.getUsername();
	    
	    Usuario usuario = usuarioDao.getUsuarioPorNome(username).get(0);
	    
	    Pet pet2 = petDao.getPetById(pet.getId()).get(0);
	    
	    if(pet2.getUsuario().getId() == usuario.getId()) {
	    	pet2.setDescricao(pet.getDescricao());
		    pet2.setIdade(pet.getIdade());
		    pet2.setNome(pet.getNome());
		    pet2.setObservacao(pet.getObservacao());
		    pet2.setTamanho(pet.getTamanho());
		    pet2.setRaca(racaDao.getRacaById(racaId).get(0));
		    pet2.setTipo(tipoDao.getTipoById(tipoId));
		    try {
				pet2.setValor(pet.getValor());
			} catch (Exception e) {

			}
		    
		    petDao.update(pet2);
	    }

		return "redirect:/pet";

	}
	
	@RequestMapping(value = "/pet/remover", method = RequestMethod.GET)
	public String removerPet(HttpServletRequest request,  @RequestParam(value = "petId",  required = true) int petId) {
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String username = user.getUsername();
		
	    System.out.println("userCadastro: "+username);
	    
	    Usuario usuario = usuarioDao.getUsuarioPorNome(username).get(0);
	    
	    Pet pet = petDao.getPetById(petId).get(0);
	    
	    if(pet.getUsuario().getId() == usuario.getId()) {
	    	petDao.remover(pet.getId());
	    }

	    return "redirect:/pet";

	}
	
	@RequestMapping(value = "/pet/disponibilidade", method = RequestMethod.GET)
	public String petIndisponivel(HttpServletRequest request,  @RequestParam(value = "petId",  required = true) int petId) {
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String username = user.getUsername();
		
	    System.out.println("userCadastro: "+username);
	    
	    Usuario usuario = usuarioDao.getUsuarioPorNome(username).get(0);
	    
	    Pet pet = petDao.getPetById(petId).get(0);
	    
	    if(pet.getUsuario().getId() == usuario.getId()) {
	    	if(pet.isDisponivel()) {
	    		pet.setDisponivel(false);
	    	}else {
	    		pet.setDisponivel(true);
	    	}
	    	petDao.update(pet);
	    }

	    return "redirect:/pet";

	}
	
	
}
