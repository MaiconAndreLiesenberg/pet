package consultorio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import consultorio.beans.Produto;
import consultorio.dao.ProdutoDao;

public class ProdutoController {

	@Autowired
	private ProdutoDao produtoDao;
	
	@RequestMapping(value = "/produto", method = RequestMethod.GET)
	public ModelAndView pet(HttpServletRequest request) {

	    List<Produto> produtos = produtoDao.getAllProduto();
	    
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("produtos", produtos);
		model.setViewName("produto");

		return model;

	}
}
