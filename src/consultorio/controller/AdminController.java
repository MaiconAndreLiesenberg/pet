package consultorio.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import consultorio.beans.Aprovacao;
import consultorio.beans.Funcionario;
import consultorio.beans.Raca;
import consultorio.beans.Tipo;
import consultorio.beans.Usuario;
import consultorio.dao.AprovacaoDao;
import consultorio.dao.AuthoritiesDao;
import consultorio.dao.FuncionarioDao;
import consultorio.dao.PetDao;
import consultorio.dao.RacaDao;
import consultorio.dao.TipoDao;
import consultorio.dao.UsuarioDao;
import consultorio.security.UserBeam;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	private UserBeam userbeam;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private JdbcUserDetailsManager jdbcUserDetailsManager;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private TipoDao tipoDao;
	
	@Autowired
	private RacaDao racaDao;
	
	@Autowired
	private FuncionarioDao funcionarioDao;
	
	@Autowired
	private AprovacaoDao aproDao;
	
	@Autowired
	private AuthoritiesDao auDao;

	/*@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.setViewName("admin");

		return model;

	}*/
	
	/*@RequestMapping(value = "/cliente", method = RequestMethod.GET)
	public ModelAndView cliente() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Custom Login Form");
		model.addObject("message", "This is protected page!");
		model.setViewName("cliente");

		return model;

	}*/
	
	@RequestMapping(value = "/aprovacao", method = RequestMethod.GET)
	public ModelAndView aprovacao(HttpServletRequest request) {
		
		List<Aprovacao> aprovacao = aproDao.getAllNAvaliados();
		
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("aprovacoes", aprovacao);
		model.setViewName("aprovacao");

		return model;

	}
	
	@RequestMapping(value = "/aprovacao/aprovar", method = RequestMethod.GET)
	public String aprovar(HttpServletRequest request, @RequestParam(value = "petId",  required = true) long petId) {
		
		Aprovacao apro = aproDao.getByIdPet(petId).get(0);
		apro.setStatus(1);
		aproDao.update(apro);
		
		return "redirect:/admin/aprovacao";

	}
	
	@RequestMapping(value = "/aprovacao/reprovar", method = RequestMethod.GET)
	public String reprovar(HttpServletRequest request, @RequestParam(value = "petId",  required = true) long petId, @RequestParam(value = "msg",  required = true) String msg) {
		
		Aprovacao apro = aproDao.getByIdPet(petId).get(0);
		apro.setStatus(2);
		apro.setMsg(msg);
		aproDao.update(apro);
		
		return "redirect:/admin/aprovacao";

	}
	
	@RequestMapping(value = "/raca", method = RequestMethod.GET)
	public ModelAndView raca(HttpServletRequest request) {
		
		List<Raca> racas = racaDao.getAllRaca();
		
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("racas", racas);
		model.setViewName("raca");

		return model;

	}
	
	@RequestMapping(value = "/raca/cadastro", method = RequestMethod.GET)
	public ModelAndView racaCadastro(HttpServletRequest request) {

		List<Tipo> tipos = tipoDao.getAllTipo();
		
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("tipos", tipos);
		model.setViewName("racaCadastro");

		return model;

	}
	
	@RequestMapping(value = "/raca/editar", method = RequestMethod.GET)
	public ModelAndView racaEditar(HttpServletRequest request, @RequestParam(value = "racaId",  required = true) long racaId) {

		Raca raca = racaDao.getRacaById(racaId).get(0);
		List<Tipo> tipos = tipoDao.getAllTipoExceptId(raca.getTipo().getId());
		
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("raca2", raca);
		model.addObject("tipos", tipos);
		model.setViewName("racaEditar");

		return model;

	}
	
	@RequestMapping(value = "/raca/editarRacaForm", method = RequestMethod.POST)
	public String editarRacaForm(HttpServletRequest request, Raca raca, long tipoId) {

		Raca raca2 = racaDao.getRacaById(raca.getId()).get(0);
		raca2.setNome(raca.getNome());
		raca2.setDescricao(raca.getDescricao());
		Tipo tipo = tipoDao.getTipoById(tipoId);
		raca2.setTipo(tipo);
		
		racaDao.update(raca2);
		
		return "redirect:/admin/raca";

	}
	
	
	@RequestMapping(value = "/raca/cadastrarRaca", method = RequestMethod.POST)
	public String cadastrarRaca(HttpServletRequest request, Raca raca, int tipoId) {
		
		Tipo tipo = tipoDao.getTipoById(tipoId);
		
		raca.setTipo(tipo);
		racaDao.add(raca);

		return "redirect:/admin/raca";

	}
	
	@RequestMapping(value = "/raca/remover", method = RequestMethod.GET)
	public String removerRaca(HttpServletRequest request, @RequestParam(value = "racaId",  required = true) long racaId) {
		
		racaDao.remove(racaId);

		return "redirect:/admin/raca";

	}
	
	@RequestMapping(value = "/tipo", method = RequestMethod.GET)
	public ModelAndView tipo(HttpServletRequest request) {

		List<Tipo> tipos = tipoDao.getAllTipo();
		
		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("tipos", tipos);
		model.setViewName("tipo");

		return model;

	}
	
	
	@RequestMapping(value = "/tipo/cadastro", method = RequestMethod.GET)
	public ModelAndView tipoCadastro(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.setViewName("tipoCadastro");

		return model;

	}
	
	@RequestMapping(value = "/tipo/cadastrarTipo", method = RequestMethod.POST)
	public String cadastrarTipo(HttpServletRequest request, Tipo tipo) {

		tipoDao.add(tipo);
		
		return "redirect:/admin/tipo";

	}
	
	@RequestMapping(value = "/tipo/editar", method = RequestMethod.GET)
	public ModelAndView editarTipo(HttpServletRequest request, @RequestParam(value = "tipoId",  required = true) int tipoId) {
		
		Tipo tipo = tipoDao.getTipoById(tipoId);

		ModelAndView model = new ModelAndView();
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("func", request.isUserInRole("ROLE_FUNC"));
		model.addObject("user", request.isUserInRole("ROLE_USER"));
		model.addObject("tipo2", tipo);
		
		model.setViewName("tipoEditar");

		return model;
	}
	
	@RequestMapping(value = "/tipo/editarTipoForm", method = RequestMethod.POST)
	public String editarTipoForm(HttpServletRequest request, Tipo tipo) {
		
		Tipo tipo2 = tipoDao.getTipoById(tipo.getId());
		tipo2.setNome(tipo.getNome());
		tipoDao.update(tipo2);
		
		return "redirect:/admin/tipo";
	}
	
	@RequestMapping(value = "/tipo/remover", method = RequestMethod.GET)
	public String removerTipo(HttpServletRequest request, @RequestParam(value = "tipoId",  required = true) int tipoId) {

		tipoDao.remove(tipoId);
		
		return "redirect:/admin/tipo";
	}
	
	@RequestMapping(value = "/funcionario", method = RequestMethod.GET)
	public ModelAndView funcionario(HttpServletRequest request) {
		
		List<Funcionario> funcionarios = funcionarioDao.getAllFuncionarios();

		ModelAndView model = new ModelAndView();
		model.setViewName("funcionario");
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		model.addObject("funcionarios", funcionarios);
		return model;

	}
	
	@RequestMapping(value = "/funcionario/cadastro", method = RequestMethod.GET)
	public ModelAndView funcionarioCadastro(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.setViewName("cadastroFuncionario");
		model.addObject("admin", request.isUserInRole("ROLE_ADMIN"));
		return model;

	}
	
	@RequestMapping(value = "/funcionario/remover", method = RequestMethod.GET)
	public String funcionarioRemover(HttpServletRequest request, @RequestParam(value = "funcId",  required = true) long funcId) {
		
		Funcionario funcionario = funcionarioDao.getFuncionarioById(funcId).get(0);
		funcionarioDao.removeFuncionario(funcionario.getId());
		auDao.removeFunc(funcionario.getUsuario().getUsername());
		
		return "redirect:/admin/funcionario";

	}
	
	@RequestMapping(value = "/funcionario/cadastrarFuncionario", method = RequestMethod.POST)
	public String castrar(HttpServletRequest request, Usuario usuario, String username, String password) {
		System.out.println("chamou");
		
		userbeam = new UserBeam();
		System.out.print("dentro do controller");
		
		List<Usuario> usuarioList = null;
		try {
			usuarioList = usuarioDao.getUsuarioPorNome(username);
		} catch (Exception e) {}
		
		if ( usuarioList == null || usuarioList.size() == 0 ) {
			
			List<String> grantedAuthorities = new ArrayList<String>(); 
			grantedAuthorities.add("ROLE_USER");
			grantedAuthorities.add("ROLE_FUNC");

			userbeam.setUsername(username);
			userbeam.setPassword(password);
			userbeam.setGrantedAuthoritiesByName(grantedAuthorities);
			userbeam.setPasswordEncoder(passwordEncoder);
			userbeam.setUserDetailsManager(jdbcUserDetailsManager);
			userbeam.setEnabled(true);
			userbeam.createUser();
			usuarioDao.add(usuario);
			
			
			Funcionario funcionario = new Funcionario();
			funcionario.setUsuario(usuario);
			funcionarioDao.add(funcionario);

		} 
		
		return "redirect:/admin/funcionario";

	}
}
